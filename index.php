<?php include 'config.php';?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<!-- bostrap css -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<!-- fontawesome -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
	<!-- fancybox -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
	<!-- datatable -->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
	<!-- sweetalert -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<style type="text/css">
		.content {
			width: 100%;
			padding: 20px;
		}
		.btn-ling{
			width: 35px; 
			height: 35px;
			border-radius: 100%; 
			padding: .25rem .25rem;
			font-size: 15px;
		}
	</style>
</head>
<body>
	<div class="wrapper">
		<div class="content">

			<div class="card mt-3">
				<div class="card-header">
					<div class="row">
						<div class="col">
							Table Produk
						</div>
						<div class="col text-right">
							<button style="font-size: 15px;" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#tambah"><i class="fas fa-plus"></i> Tambah</button>
						</div>
					</div>
				</div>
				<div class="card-body card-font">
					<table id="example" class="table table-striped table-bordered table-hover" style="width: 100%;">
						<thead>
							<tr>
								<th style="max-width:35px;">No.</th>
								<th>Nama Barang</th>
								<th>Keterangan</th>
								<th>Harga</th>
								<th>Jumlah</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$nom = 1;
							$kode = mysqli_query($config,"SELECT * FROM produk");
							while ($dat = mysqli_fetch_array($kode)) {
								?>
								<tr>
									<td>
										<?php echo $nom++; ?>
									</td>
									<td>
										<?php echo $dat['nama_produk']; ?>
									</td>
									<td>
										<?php echo $dat['keterangan'];?>
									</td>
									<td>
										<?php echo $dat['harga']; ?>
									</td>
									<td style="text-align: center;">
										<?php echo $dat['jumlah']; ?>
									</td>
									<td class="text-center">
										<button class="btn btn-outline-primary btn-sm btn-ling edit-modal" data-nama="<?php echo $dat['nama_produk']; ?>" data-ket="<?php echo $dat['keterangan']; ?>" data-harga="<?php echo $dat['harga']; ?>" data-jumlah="<?php echo $dat['jumlah']; ?>">
											<i class="fas fa-user-edit"></i>
										</button>
										<button class="btn btn-outline-danger btn-sm btn-ling delete-modal" data-name="<?php echo $dat['nama_produk']; ?>">
											<i class="fas fa-trash-alt"></i>
										</button>
									</td>
								</tr>

							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</div>

	<!-- Modal create data -->
	<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title" id="exampleModalLabel">
						Tambah Produk
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form method="post" action="tambah.php">
					<div class="modal-body">

						<div class="form-group">
							<label>Nama Barang</label>
							<input type="text" class="form-control form-control-sm" name="nama" required>
						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Harga</label>
								<input type="text" class="number form-control form-control-sm" name="harga" required>
							</div>
							<div class="form-group col-md-6">
								<label>Jumlah</label>
								<input type="text" class="number form-control form-control-sm" name="jumlah" required>
							</div>
						</div>

						<div class="form-group">
							<label>Keterangan</label>
							<textarea name="keterangan" class="form-control form-control-sm" rows="3"></textarea>
						</div>

					</div>
					<div class="modal-footer">
						<button type="submit" id="m" class="btn btn-primary btn-sm">Tambah</button>
						<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- end modal create -->


	<!-- modal edit -->
	<div class="modal fade" id="editModal" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title" id="exampleModalLabel">
						Edit Produk
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form method="post" action="edit.php">
					<div class="modal-body">
						<div class="form-group">
							<label>Nama Barang</label>
							<input id="nama_sebelum" type="hidden" name="nama_sebelum" required>
							<input id="nama" type="text" class="form-control form-control-sm" name="nama" required>
						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Harga</label>
								<input id="harga" type="text" class="number form-control form-control-sm" name="harga" required>
							</div>
							<div class="form-group col-md-6">
								<label>Jumlah</label>
								<input id="jumlah" type="text" class="number form-control form-control-sm" name="jumlah" required>
							</div>
						</div>

						<div class="form-group">
							<label>Keterangan</label>
							<textarea id="ket" name="keterangan" class="form-control form-control-sm" rows="3"></textarea>
						</div>

					</div>
					<div class="modal-footer">
						<button type="submit" id="m" class="btn btn-primary btn-sm">Simpan</button>
						<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- end modal delete -->


	<!-- modal delete -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title" id="exampleModalLabel">
						Hapus Data
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form action="hapus.php" method="post">
					<div class="modal-body">
						Anda Yakin Untuk Menghapus Data <span class="dname"></span> ??
						<input type="hidden" class="nama" name="name">
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i> Delete</button>
						<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- end modal delete -->

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>

	<script type="text/javascript">
		$(document).ready(function () {
			var table = $('#example').DataTable( {
				rowReorder: {
					selector: 'td:nth-child(2)'
				},
				responsive: true,
				"pagingType": "full_numbers",
				language: {
					oPaginate: {
						sNext: '<i class="fas fa-chevron-right"></i>',
						sPrevious: '<i class="fas fa-chevron-left"></i>',
						sFirst: '<i class="fas fa-angle-double-left"></i>',
						sLast: '<i class="fas fa-angle-double-right"></i>'
					}
				}  
			});
        	// set form search
        	var cari = document.getElementsByTagName("input");
        	cari[0].setAttribute('size','10');
        	// end

        	// set size pagination
        	$('.col-md-7').addClass('pagination-sm');
        	// end
        });
		$(document).on('click', '.delete-modal', function() {
			$('.nama').val($(this).data('name'));
			$('.dname').html($(this).data('name'));
			$('#myModal').modal('show');
		});

		$(document).on('click', '.edit-modal', function() {
			$('#nama_sebelum').val($(this).data('nama'));
			$('#nama').val($(this).data('nama'));
			$('#harga').val($(this).data('harga'));
			$('#jumlah').val($(this).data('jumlah'));
			$('#ket').text($(this).data('ket'));
			$('#editModal').modal('show');
		});

		$('.number').on('keydown', function(e){
			-1!==$
			.inArray(e.keyCode,[46,8,9,27,13,110,190]) || /65|67|86|88/
			.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey)
			|| 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey|| 48 > e.keyCode || 57 < e.keyCode)
			&& (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
		});
	</script>
</body>
</html>